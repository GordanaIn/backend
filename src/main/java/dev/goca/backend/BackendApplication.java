package dev.goca.backend;

import dev.goca.backend.entity.Consult;
import dev.goca.backend.repository.ConsultRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"dev.goca.backend.controller","dev.goca.backend.repository"})
public class BackendApplication implements CommandLineRunner {

    public BackendApplication(ConsultRepository consultRepository) {
        this.consultRepository = consultRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);

    }

    private final ConsultRepository consultRepository;

    @Override
    public void run(String... args) throws Exception{
        Consult consult= new Consult();
        consult.setFirstName("Mimi");
        consult.setLastName("Larsson");
        consult.setEmail("mimilarsson@hotmail.com");
        consultRepository.save(consult);

        Consult consult_2= new Consult();
        consult_2.setFirstName("Pet");
        consult_2.setLastName("Smith");
        consult_2.setEmail("smithpet@hotmail.com");
        consultRepository.save(consult_2);
    }
}
