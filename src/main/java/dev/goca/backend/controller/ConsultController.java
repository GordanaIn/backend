package dev.goca.backend.controller;

import dev.goca.backend.entity.Consult;
import dev.goca.backend.service.ConsultService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;



@RequestMapping(value = "/consults")
public class ConsultController {


     ConsultService service;


    @PostMapping("/create")
    public ResponseEntity<Consult> createConsult(@RequestBody Consult consult){
         Consult createConsult = service.createNewConsult(consult);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createConsult.getId())
                .toUri();
        return ResponseEntity.created(uri).body(createConsult);
    }

    @GetMapping("/all")
    public List<Consult> getAllCunsults(){
     return service.getConsults();
    }

    @GetMapping("/read/{id}")
    public ResponseEntity<Consult> getConsultById(@PathVariable(value= "id")Long id ) throws Exception{
           Consult consult = service.getConsultById(id);
             return ResponseEntity.ok(consult);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Consult> updateConsult(@PathVariable(value = "id")Long id,
                                                 @RequestBody Consult consult){
       Consult updateconsult = service.getConsultById(id);
        updateconsult.setId(consult.getId());
        updateconsult.setFirstName(consult.getFirstName());
        updateconsult.setLastName(consult.getLastName());
        updateconsult.setEmail(consult.getEmail());
        updateconsult.setProfession(consult.getProfession());
        updateconsult.setContactNumber(consult.getContactNumber());

       service.createNewConsult(consult);

       return ResponseEntity.ok(consult);

    }


    @DeleteMapping("/delete/{id}")
    public void deleteConsult(@PathVariable(value = "id") Long id){
       service.deleteConsult(id);
    }


}
