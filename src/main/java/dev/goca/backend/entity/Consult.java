package dev.goca.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name="consults")
@NoArgsConstructor
public class Consult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email ;

    @Column(name = "profession")
    private String profession;

    @Column(name = "contact")
    private String contactNumber;



    public Consult(Long id,
                    String firstName,
                    String lastName,
                    String email,
                    String profession,
                    String contactNumber) {
        this.id=id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profession = profession;
        this.contactNumber = contactNumber;
    }



}
