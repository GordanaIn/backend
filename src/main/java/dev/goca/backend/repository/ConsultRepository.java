package dev.goca.backend.repository;

import dev.goca.backend.entity.Consult;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;


@Repository
public interface ConsultRepository extends CrudRepository<Consult, Long> {

    List<Consult>findConsultById(Long id);



}
