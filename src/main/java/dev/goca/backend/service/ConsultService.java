package dev.goca.backend.service;

import dev.goca.backend.entity.Consult;

import java.util.List;

public interface ConsultService {
    // Get a List of Consults
    List<Consult> getConsults();

    Consult getConsultById(Long id);

    void deleteConsult(Long id);

    Consult createNewConsult(Consult consult);

    void updateConsultDetails(Long id, Consult consult);
}
