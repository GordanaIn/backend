package dev.goca.backend.service;

import dev.goca.backend.entity.Consult;
import dev.goca.backend.repository.ConsultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ConsultServiceImplement implements ConsultService {

    @Autowired
    ConsultRepository repo;


    @Override
    public Consult createNewConsult(Consult consult) {
        return repo.save(consult);
    }

    // Get a List of Consults
    @Override
    public List<Consult> getConsults(){
        return (List<Consult>) repo.findAll();
    }
    @Override
    public Consult getConsultById(Long id){
        List<Consult> consult = repo.findConsultById(id);
        return consult.get(Math.toIntExact(id));
   }

   @Override
   public void updateConsultDetails(Long id, Consult consult){
        repo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
       consult.setId(id);
       repo.save(consult);
   }
   @Override
    public void deleteConsult(Long id){
        repo.findById(id).orElseThrow(EntityNotFoundException::new);
        repo.deleteById(id);
    }

}
