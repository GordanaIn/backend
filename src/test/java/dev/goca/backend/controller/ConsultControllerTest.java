/*package dev.goca.backend.controller;

import dev.goca.backend.entity.Consult;
import dev.goca.backend.repository.ConsultRepository;
import dev.goca.backend.service.ConsultService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integrationtest")
@ContextConfiguration
class ConsultControllerTest {


    @Autowired
    private ConsultRepository repo;

    @MockBean
    private ConsultService consultService;

    @MockBean
    private ConsultRepository consultRepository;

    */
/*@Autowired
    private ObjectMapper objectMapper;*//*



    @MockBean
    MockMvc mockMvc;


    private TestRestTemplate testRestTemplate;

    @LocalServerPort private int port;
    private String testUrl(String s) {
        return "http://localhost:"+port+ "/api/consults";
    }

    Consult consult;
    @BeforeEach
    void setUp() {
        testRestTemplate = new TestRestTemplate();
        consultRepository.deleteAll();
    }

    @Test
    void createConsult() throws Exception{
        testRestTemplate.put(testUrl("/consults"),new Consult(1L, "Peter"," Karlson", "peterkarlson@gmail.com","Java developer","0736454546"),Consult.class);

        // Then
        Consult consult = (Consult) consultRepository.findAll();
        assertEquals("Peter ", consult.getFirstName());
        assertEquals("Karlson", consult.getLastName());



    }

    @Test
    void getAllCunsults() {
       // Given
        consultRepository.save(new Consult(1L, "Peter"," Karlson", "peterkarlson@gmail.com","Java developer","0736454546"));
        consultRepository.save(new Consult(2L, "Peter"," Karlson", "peterkarlson@gmail.com","Java developer","0736454546"));
        // When
        ResponseEntity<Consult> forEntity = testRestTemplate.getForEntity(testUrl("/consults"), Consult.class);

        // Then
        assertEquals(200, forEntity.getStatusCodeValue());
        Consult consults = forEntity.getBody();
        assert consults != null;
        assertEquals(1L, consults.getId());
        assertEquals("Peter", consults.getFirstName());
        assertEquals("Karlson", consults.getLastName());
        assertEquals("peterkarlson@gmail.com", consults.getEmail());
        assertEquals("Java developer", consults.getProfession());
        assertEquals("0736454546", consults.getContactNumber());
    }



  */
/*  @Test
    void getConsultById() {

        List<Consult> consultList = new ArrayList<>();
        consultList.add(new Consult(1L,"Mimi","Poss" ,"mimiposs@git.com", "JavaDeveloper", "070893746"));
        consultList.add(new Consult(2L,"Mimi","Poss" ,"mimiposs@git.com", "JavaDeveloper", "070893746"));
        consultList.add(new Consult(3L,"Mimi","Poss" ,"mimiposs@git.com", "JavaDeveloper", "070893746"));
        consultList.add(new Consult(4L,"Mimi","Poss" ,"mimiposs@git.com", "JavaDeveloper", "070893746"));
        Mockito.when(consultRepository.findAll().toString());

       // MvcResult mvcResult= mockMvc.perform(consultList.get()).andExpect(status().isOk()).andReturn();

    }*//*




*/
/*
    @Test
    void updateConsult() {
    }

    @Test
    void deleteConsult() {
    }

}
*/
